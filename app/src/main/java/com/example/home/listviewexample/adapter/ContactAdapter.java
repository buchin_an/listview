package com.example.home.listviewexample.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.home.listviewexample.ContactList;
import com.example.home.listviewexample.R;
import com.example.home.listviewexample.model.Contact;

import java.util.List;

public class ContactAdapter extends BaseAdapter {


    private Context context;
    List<Contact> contacts = ContactList.getAllContact();

    public ContactAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return contacts.size();
    }

    @Override
    public Object getItem(int position) {
        return contacts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = convertView;
        ViewHolder holder;
        if (rowView == null) {
            rowView = LayoutInflater.from(context)
                    .inflate(R.layout.item_contact, parent, false);
            holder = new ViewHolder();
            holder.icon = rowView.findViewById(R.id.contact_image);
            holder.name = rowView.findViewById(R.id.contact_name);
            holder.email = rowView.findViewById(R.id.contact_address);
            rowView.setTag(holder);

        } else {
            holder = (ViewHolder) rowView.getTag();
        }
        holder.email.setText(contacts.get(position).getEmail());
        holder.name.setText(contacts.get(position).getName());
        holder.icon.setImageResource(contacts.get(position).getIcon());
        return rowView;
    }

    static class ViewHolder {
        ImageView icon;
        TextView name;
        TextView email;
    }
}
