package com.example.home.listviewexample;

import com.example.home.listviewexample.model.Contact;

import java.util.ArrayList;
import java.util.List;

public final class DefaultContacts {

    public static List getDefaultContacts() {
        List<Contact> contacts = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            contacts.add(new Contact("name" + i, "email" + i,
                    "address" + i, "phone" + i, R.mipmap.ic_launcher_round));
        }
        return contacts;
    }
}
