package com.example.home.listviewexample;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import com.example.home.listviewexample.adapter.ContactAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;

public class MainActivity extends AppCompatActivity {

    public static final String KEY = "key";
    @BindView(R.id.list)
    ListView listView;

    public static void start(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        context.startActivity(intent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        listView.setAdapter(new ContactAdapter(this));
    }

    @OnClick(R.id.fab)
    public void addItem() {
        AddContactActivity.start(this);
    }

    @OnItemClick(R.id.list)
    public void onItemSelect(int position) {
        ContactActivity.start(this, ContactList.getContact(position));
    }


}
