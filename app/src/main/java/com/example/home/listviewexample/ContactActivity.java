package com.example.home.listviewexample;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.home.listviewexample.model.Contact;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContactActivity extends AppCompatActivity {

    private Contact contact;
    @BindView(R.id.contact_image)
    ImageView image;
    @BindView(R.id.contact_name)
    TextView name;
    @BindView(R.id.contact_email)
    TextView email;
    @BindView(R.id.contact_address)
    TextView address;
    @BindView(R.id.contact_phone)
    TextView phone;


    public static void start(Context context, Contact contact) {
        Intent intent = new Intent(context, ContactActivity.class);
        intent.putExtra(MainActivity.KEY, contact);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        ButterKnife.bind(this);

        contact = (Contact) getIntent().getSerializableExtra(MainActivity.KEY);
        image.setImageResource(R.mipmap.ic_launcher_round);
        name.setText(contact.getName());
        email.setText(contact.getEmail());
        address.setText(contact.getAddress());
        phone.setText(contact.getPhone());
    }
}
