package com.example.home.listviewexample;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import com.example.home.listviewexample.model.Contact;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddContactActivity extends AppCompatActivity {

    @BindView(R.id.add_name)
    EditText name;
    @BindView(R.id.add_address)
    EditText address;
    @BindView(R.id.add_phone)
    EditText phone;
    @BindView(R.id.add_email)
    EditText email;

    public static void start(Context context) {
        Intent intent = new Intent(context, AddContactActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);
        ButterKnife.bind(this);

    }

    @OnClick(R.id.add_button)
    public void addContact() {
        ContactList.addContact(new Contact(name.getText().toString(),
                email.getText().toString(), address.getText().toString(),
                phone.getText().toString(), R.mipmap.ic_launcher_round));
        MainActivity.start(this);
    }

}
