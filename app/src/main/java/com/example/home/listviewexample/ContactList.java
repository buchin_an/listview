package com.example.home.listviewexample;

import com.example.home.listviewexample.model.Contact;

import java.util.List;

public class ContactList {

    private static List<Contact> contacts = DefaultContacts.getDefaultContacts();

    public static Contact getContact(int position) {
        return contacts.get(position);
    }

    public static void addContact(Contact contact) {
        contacts.add(contact);
    }

    public static List<Contact> getAllContact() {
        return contacts;
    }
}
